# Content Creator Guide

## Adding a Course Category on the Home Page
1. In the root folder, open up `index.html`.
2. To add a course category, find the second `<section> tag`.
3. Follow the example `<h2> tag` semantics within it to create a new course category.
4. Follow the example `categories container` semantics to create a container to hold the courses within this category.
5. Add `<br> tags` as required to make the page visually pleasing.

## Adding / Editing a Course on the Home Page
1. In the root folder, open up `index.html`.
2. To add a course category, find the second `<section> tag`.
3. Find the `categories container`
4. Edit it to reflect the required changes (Addition/Edition of Course).
5. Store the course image asset in the folder `./assets/img/courses/`

## Initialising a Course to start writing content
For this example let's assume the course is on `Angular`

Create a new folder in the root directory with the name of the course in all lowercase. In this example create a folder called `angular`. For this course the url to access it will be `https://coder.guide/angular/<first-lesson-url>`.

## Creating Lessons.
1. Copy the folder in path `./dart/basic-syntax` and paste it into your newly created course folder. In this example it will be inside `./angular` so that there is a folder in the path `./angular/basic-syntax` with contents inside it. 
2. Rename `basic-syntax` to the url you wish that the first lesson should have. Ensure that the naming is all lowercase alphabets, numbers, and the only special character being permitted is '-'.
3. Open up `./<course-name>/<lesson-url>/config.js`
4. Make changes to all the data here to be reflected in this lesson page.
5. If this is your first lesson, inside `config.js`, set `previous_lesson_link` to `NONE`. Note that this is case-sensitive.
6. If this is your last lesson, inside `config.js`, set `next_lesson_link` to `NONE`. Note that this is case-sensitive.
7. You can create the main article by heading over to `index.html` in the same folder.
8. Inside `index.html` find the `<article id="content"> tag`. To write the article you will only make changes within this `<article>` tag. 
9. You should already have example syntax to create headings, paragraphs, images, tables, code syntax and lists. Use these elements to construct the articles page.
10. If this is your first lesson, set the course access url to `https://coder.guide/<course-name>/<lesson-url>` of the first lesson.
11. Inside `index.html` find the text `"ace/mode/javascript"` and change it to `"ace/mode/<your-syntax highlighting-language>"`, to enable auto-syntax highlighting and formatting to your current language.
12. Inside `index.html`, Edit the `<meta name="description"> tag`, `<meta name="author"> tag` and the title tag to reflect the current author and lesson being taught.
