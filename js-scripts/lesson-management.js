function load_data(config) {

    document.getElementById('course_name').innerHTML = config.course_name;
    document.getElementById('lecture_slogan').innerHTML = config.lecture_slogan;
    document.getElementById('author').innerHTML = config.author;
    document.getElementById('author_initials').innerHTML = config.author_initials;
    document.getElementById('date_published').innerHTML = config.date_published;

    document.getElementById('featured_course_course_image').src = config.featured_course.course_image;
    document.getElementById('featured_course_course_name').innerHTML = config.featured_course.course_name;
    document.getElementById('featured_course_course_caption').innerHTML = config.featured_course.course_caption;
    document.getElementById('featured_course_course_link').onclick = function() {
        goto_course(config.featured_course.course_link)
    }

    let nll = config.lesson_navigation.next_lesson_link;
    let pll = config.lesson_navigation.previous_lesson_link;

    if (nll === "NONE"){
        document.getElementById('next_lesson_link').disabled = true;
    }else{
        document.getElementById('next_lesson_link').onclick = function() {
            goto_course_in_place(nll);
        }
    }

    if (pll === "NONE"){
        document.getElementById('previous_lesson_link').disabled = true;
    }else{
        document.getElementById('previous_lesson_link').onclick = function() {
            goto_course_in_place(pll);
        }
    }


    let course_navigation = "";

    for(let i = 0; i < config.course_navigation.length ; i++){
        course_navigation += '\n';
        course_navigation += '<a href="' + config.course_navigation[i].url +'" data-scroll-to data-scroll-to-offset="50" class="list-group-item list-group-item-action d-flex justify-content-between px-4">';
        course_navigation += '<div>';
        course_navigation += '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>';
        course_navigation += '<span>' + config.course_navigation[i].label + '</span>';
        course_navigation += '</div>';
        course_navigation += '</div>';
        course_navigation += '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>';
        course_navigation += '</div>';
        course_navigation += '</a>';
        course_navigation += '\n';
    }

    document.getElementById('toc').innerHTML += course_navigation;


    let course_tags = "";
    for(let i = 0; i < config.tags.length ; i++) {
        course_tags += '<li class="list-inline-item pr-2">';
        course_tags += '<span class="badge badge-pill badge-' + config.tags[i].colour + '">' + config.tags[i].name + '</span>';
        course_tags += '</li>';
    }

    document.getElementById('tags').innerHTML += course_tags;

}

function goto_course(url) {
    window.open(url, '_blank').focus();
}

function goto_course_in_place(url) {
    window.open(url, '_self').focus();
}
