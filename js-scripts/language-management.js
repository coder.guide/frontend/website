String.prototype.format = function() {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
}

var template_html_article_entry_for_all_articles_view = '<div class="card">\n' +
    '        <div class="card-body row align-items-center">\n' +
    '            <div class="col-md-6 col">\n' +
    '                <div class="media align-items-center">\n' +
    '                    <div>\n' +
    '                            <span class="badge badge-lg badge-dot mr-3">\n' +
    '                                <i class="{0}"></i>\n' +
    '                            </span>\n' +
    '                    </div>\n' +
    '                    <div class="media-body">\n' +
    '                        <a href="./{1}" class="h6 stretched-link mb-0">{2}</a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-6">\n' +
    '                <!-- Delimiter (for small resolutions) -->\n' +
    '                <hr class="mt-3 mb-4 d-md-none">\n' +
    '                <!-- Topic info -->\n' +
    '                <div class="row align-items-center">\n' +
    '                    <div class="col">\n' +
    '                    </div>\n' +
    '                    <div class="col-auto">\n' +
    '                        <a href="#" class="stretched-link mr-4">\n' +
    '                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"\n' +
    '                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"\n' +
    '                                 stroke-linejoin="round" class="feather feather-bar-chart-2">\n' +
    '                                <line x1="18" y1="20" x2="18" y2="10"></line>\n' +
    '                                <line x1="12" y1="20" x2="12" y2="4"></line>\n' +
    '                                <line x1="6" y1="20" x2="6" y2="14"></line>\n' +
    '                            </svg>\n' +
    '                            <span class="h6 text-sm ml-3">{3}+ Views</span>\n' +
    '                        </a>\n' +
    '                        <a href="#" class="stretched-link">\n' +
    '                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"\n' +
    '                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"\n' +
    '                                 stroke-linejoin="round" class="feather feather-calendar">\n' +
    '                                <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>\n' +
    '                                <line x1="16" y1="2" x2="16" y2="6"></line>\n' +
    '                                <line x1="8" y1="2" x2="8" y2="6"></line>\n' +
    '                                <line x1="3" y1="10" x2="21" y2="10"></line>\n' +
    '                            </svg>\n' +
    '                            <span class="h6 text-sm ml-3">{4} days ago</span>\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>';

let blimp_color = {
    blue: "bg-primary",
    red: "bg-danger",
    green: "bg-success"
}

// Data Centre
let articles = [
    {
        blimp_color: blimp_color.green,
        article_link: "how-to-create-a-linked-list-in-dart",
        article_name: "How to create a linked list in Dart",
        article_total_views: "5000",
        article_last_updated: "4",
    },
    {
        blimp_color: blimp_color.blue,
        article_link: "how-to-create-a-linked-list-in-dart",
        article_name: "How to create a linked list in Dart",
        article_total_views: "50000",
        article_last_updated: "2",
    }
]

function render_articles_to_page(){

    let articles_as_html = "";

    for(let i = articles.length-1; i >= 0 ; i--){
        articles_as_html += template_html_article_entry_for_all_articles_view.format(
            articles[i].blimp_color,
            articles[i].article_link,
            articles[i].article_name,
            articles[i].article_total_views,
            articles[i].article_last_updated
        )
    }

    document.getElementById('articles').innerHTML = articles_as_html;

}
