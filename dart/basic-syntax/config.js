let config = {
    course_name: "Dart For Complete Beginners",
    lecture_slogan: "Basic Dart Syntax To Get You Started",
    tags: [
        {
            name: 'Dart Lang',
            colour: 'primary'
        },
        {
            name: 'Basic Syntax',
            colour: 'success'
        },
        {
            name: 'Environment Setup',
            colour: 'danger'
        }
    ],
    author: "Anna Schrödinger",
    author_initials: "AS",
    date_published: "25th July 2021",
    featured_course: {
        course_image: "https://img-c.udemycdn.com/course/240x135/3869602_2c7f_2.jpg",
        course_name: "Python Coding Masterclass",
        course_caption: "Prefer video based learning? We have a course for you!",
        course_link: "https://www.exceedcourses.com/course/python/"
    },
    lesson_navigation: {
        previous_lesson_link: "NONE",
        next_lesson_link: "https://coder.guide/dart/data-types"
    },
    course_navigation: [
        {
            label: "Basic Syntax",
            url: "https://coder.guide/dart/basic-syntax"
        },
        {
            label: "Data Types",
            url: "https://coder.guide/dart/data-types"
        },
        {
            label: "Operators",
            url: "https://coder.guide/dart/operators"
        },
        {
            label: "Loops",
            url: "https://coder.guide/dart/loops"
        },
        {
            label: "Decision Making",
            url: "https://coder.guide/dart/decision-making"
        },
        {
            label: "Numeric Computations",
            url: "https://coder.guide/dart/numeric-computations"
        },

        {
            label: "String Operations",
            url: "https://coder.guide/dart/string-operations"
        },
        {
            label: "Runes",
            url: "https://coder.guide/dart/runes"
        },
        {
            label: "Functions",
            url: "https://coder.guide/dart/functions"
        },
        {
            label: "Interfaces",
            url: "https://coder.guide/dart/interfaces"
        },
        {
            label: "Classes",
            url: "https://coder.guide/dart/classes"
        },
    ]
}
